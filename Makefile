all:
	lualatex -halt-on-error LIVRO.tex
	lualatex -halt-on-error LIVRO.tex
test:
	lualatex LIVRO.tex
	lualatex LIVRO.tex
	evince LIVRO.pdf
clean:
	rm *aux *log *tui *toc
